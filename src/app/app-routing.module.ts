import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './modules/general/home/home.component';
import {NotFoundComponent} from './modules/general/not-found/not-found.component';

const routes: Routes = [
  {path: '', redirectTo: 'app/general/task', pathMatch: 'full'}, // redirect to
  {path: '', component: HomeComponent},
  {
    path: 'app',
    loadChildren: () => import('./modules/layout/layout.module')
      .then(mod => mod.LayoutModule)
  },
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
