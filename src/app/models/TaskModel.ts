export class TaskModel {
  id: number = null;
  subject: string = '';
  description: string = '';
  dueDate: string = '';
  priority: string = '';
  completed: boolean = false;
}
