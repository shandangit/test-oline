"use strict";
exports.__esModule = true;
exports.TaskModel = void 0;
var TaskModel = /** @class */ (function () {
    function TaskModel() {
        this.id = null;
        this.subject = '';
        this.description = '';
        this.dueDate = '';
        this.priority = '';
        this.completed = false;
    }
    return TaskModel;
}());
exports.TaskModel = TaskModel;
//# sourceMappingURL=TaskModel.js.map