import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable()
export class SettingService {
  constructor(
    private snackBar: MatSnackBar
  ) {
  }

  openNotification(message): void {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }
}
