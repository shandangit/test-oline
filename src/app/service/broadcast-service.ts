import {Observable, Subject} from 'rxjs/index';
import {Injectable} from '@angular/core';

;
import {filter, map} from 'rxjs/operators';

interface OnCachedEvent {
  key: any;
  data?: any;
}

@Injectable({
  providedIn: 'root'
})
export class BroadCastService {
  // define data global for app
  private eventBus: Subject<OnCachedEvent>;

  constructor() {
    this.eventBus = new Subject<OnCachedEvent>();
  }

  setBroadcast(key: any, data?: any) {
    this.eventBus.next({key, data});
  }

  onBroadcast<T>(key: any): Observable<T> {
    return this.eventBus.asObservable().pipe(
      filter(event => event.key === key),
      map(event => <T> event.data)
    );
  }
}
