import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  private keyCache = 'task_store';
  private cacheData = [];

  constructor() {
  }

  // Get list Tasks
  public listTaskData(): any {
    let taskCache = [];
    const cache = localStorage.getItem(this.keyCache);
    if (cache !== undefined) {
      taskCache = JSON.parse(cache) || [];
      taskCache.sort((a, b) => {
        const dateA = new Date(a.dueDate).getTime();
        const dateB = new Date(b.dueDate).getTime();
        return (dateA - dateB);
      });
      this.cacheData = taskCache;
    }
    return taskCache;
  }

  // Save task to LocalStorage
  private storeData(data): void {
    localStorage.setItem(this.keyCache, JSON.stringify(data));
  }

  // Save task
  public saveTask(item): void {
    this.cacheData = this.listTaskData();
    if (this.cacheData && this.cacheData.length > 0) {
      const itemMax = this.cacheData.reduce((prev, current) => {
        if (+current.id > +prev.id) {
          return current;
        } else {
          return prev;
        }
      });
      item['id'] = itemMax.id + 1;
      this.cacheData.push(item);
    } else {
      item['id'] = 1;
      this.cacheData.push(item);
    }
    this.storeData(this.cacheData);
  }

  // Update task
  public editTask(item): void {
    this.cacheData = this.cacheData.map(v => {
      if (v.id === item.id) {
        v = item;
      }
      return v;
    });
    this.storeData(this.cacheData);
  }

  // Remove task by ID
  public deleteTask(ids): void {
    this.cacheData = this.cacheData.filter(v => !ids.includes(v.id));
    this.storeData(this.cacheData);
  }
}
