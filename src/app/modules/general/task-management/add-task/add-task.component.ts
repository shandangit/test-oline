import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GeneralService} from '../../general.service';
import {BroadCastService} from '../../../../service/broadcast-service';
import {SettingService} from '../../../../service/setting.service';
import * as moment from 'moment';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit, OnDestroy {
  public listPriority = [
    {key: 'normal'},
    {key: 'low'},
    {key: 'high'},
  ];
  public addForm: FormGroup;
  public submitted = false;
  public minDate = new Date();

  constructor(
    private formBuilder: FormBuilder,
    private service: GeneralService,
    private broadCast: BroadCastService,
    private setting: SettingService
  ) {
    this.createForm();
  }

  ngOnInit(): void {
  }

  private createForm(): void {
    this.addForm = this.formBuilder.group({
      subject: ['', [Validators.required]],
      description: [''],
      dueDate: [this.minDate],
      priority: ['normal', [Validators.required]],
    });
  }

  get f() {
    return this.addForm.controls;
  }

  onSubmitTask(): void {
    this.submitted = true;
    // validate form
    if (this.addForm.invalid) {
      return;
    }
    const formData = this.addForm.value;
    const body = {
      id: null,
      subject: formData.subject,
      description: formData.description,
      dueDate: formData.dueDate,
      priority: formData.priority,
      completed: false
    };
    this.submitted = false;
    this.service.saveTask(body);
    // init form again
    this.createForm();
    // notification
    this.setting.openNotification('Add success');
    this.broadCast.setBroadcast('add_success', true);
  }

  ngOnDestroy(): void {
  }

}
