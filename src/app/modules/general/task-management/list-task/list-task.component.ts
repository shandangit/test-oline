import {Component, OnInit} from '@angular/core';
import {GeneralService} from '../../general.service';
import {BroadCastService} from '../../../../service/broadcast-service';
import * as $ from 'jquery';
import {SettingService} from '../../../../service/setting.service';

@Component({
  selector: 'app-list-task',
  templateUrl: './list-task.component.html',
  styleUrls: ['./list-task.component.scss']
})
export class ListTaskComponent implements OnInit {
  public listTask = [];
  public timestamp = new Date().getTime();
  public keySearch = '';
  public isShowBulk = false;
  public listBulk = [];

  constructor(
    private service: GeneralService,
    private broadCast: BroadCastService,
    private setting: SettingService
  ) {
    this.listTask = this.service.listTaskData();
  }

  ngOnInit(): void {
    this.broadCast.onBroadcast('add_success').subscribe(res => {
      this.listTask = this.service.listTaskData();
    });
  }

  collapsibleTab(id): void {
    const ele = '#expander' + id;
    if ($(ele).hasClass('show')) {
      $(ele).removeClass('show');
    } else {
      $(ele).addClass('show');
    }
  }

  // On update task
  onUpdateTask(item): void {
    this.collapsibleTab(item.id);
  }

  // On remove task
  onRemoveTask(item): void {
    this.service.deleteTask([item.id]);
    this.listTask = this.service.listTaskData();
    this.setting.openNotification('Remove success');
  }

  // On remove task
  onRemoveAllTask(): void {
    let ids = [];
    this.listTask.map(v => {
      if (v.completed) {
        ids.push(v.id);
      }
    });
    if (ids.length > 0) {
      this.service.deleteTask(ids);
      this.setting.openNotification('Remove success');
      this.listTask = this.service.listTaskData();
      this.isShowBulk = false;
    }
  }

  selectedAll(item): void {
    if (item.completed) {
      this.isShowBulk = true;
    } else {
      let checkCompleted = 0;
      this.listTask.map(v => {
        if (v.completed) {
          checkCompleted += 1;
        }
      });
      if (checkCompleted <= 0) {
        this.isShowBulk = false;
      }
    }
  }

}
