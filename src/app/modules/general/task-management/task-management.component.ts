import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-task-management',
  templateUrl: './task-management.component.html',
  styleUrls: ['./task-management.component.scss']
})
export class TaskManagementComponent implements OnInit {
  public tabIndex = 0;

  constructor() {
  }

  ngOnInit(): void {
  }

  public onChangeTab(value): void {
    this.tabIndex = value;
  }

}
