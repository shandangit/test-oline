import {Component, Input, OnInit} from '@angular/core';
import {GeneralService} from '../../general.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BroadCastService} from '../../../../service/broadcast-service';
import {SettingService} from '../../../../service/setting.service';

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.scss']
})
export class UpdateTaskComponent implements OnInit {
  @Input()
  set data(val) {
    if (val) {
      if (Object.keys(val.data).length !== 0 && val.data.dueDate !== undefined) {
        this.fillForm(val.data.id, val.data.subject, val.data.description, val.data.dueDate, val.data.priority);
      }
    }
  };

  public listPriority = [
    {key: 'normal'},
    {key: 'low'},
    {key: 'high'},
  ];
  public dataUpdate: FormGroup;
  public submitted = false;
  public minDate = new Date();

  constructor(
    private formBuilder: FormBuilder,
    private service: GeneralService,
    private broadCast: BroadCastService,
    private setting: SettingService
  ) {
  }

  ngOnInit(): void {
  }

  private fillForm(id, sub, des, date, prio): void {
    this.minDate = (new Date(date).getTime() > this.minDate.getTime()) ? this.minDate : date;
    this.dataUpdate = this.formBuilder.group({
      id: [id, [Validators.required]],
      subject: [sub, [Validators.required]],
      description: [des],
      dueDate: [date],
      priority: [prio, [Validators.required]],
    });
  }

  get f() {
    return this.dataUpdate.controls;
  }

  public onUpdateTask(): void {
    this.submitted = true;
    if (this.dataUpdate.invalid) {
      return;
    }
    const formData = this.dataUpdate.value;
    this.service.editTask(formData);
    // notification
    this.setting.openNotification('Update success');
    this.broadCast.setBroadcast('add_success', true);
  }

}
