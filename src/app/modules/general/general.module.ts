import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {GeneralRoutingModule} from './general-routing.module';
import {TaskManagementComponent} from './task-management/task-management.component';
import {GeneralService} from './general.service';
import {AddTaskComponent} from './task-management/add-task/add-task.component';
import {ListTaskComponent} from './task-management/list-task/list-task.component';
import {SharesModule} from '../../shares/shares.module';
import {UpdateTaskComponent} from './task-management/update-task/update-task.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatButtonModule} from '@angular/material/button';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'DD MMM YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@NgModule({
  declarations: [
    TaskManagementComponent,
    AddTaskComponent,
    ListTaskComponent,
    UpdateTaskComponent
  ],
  imports: [
    CommonModule,
    SharesModule,
    GeneralRoutingModule,
    MatDatepickerModule,
    MatButtonModule,
    MatInputModule,
    MatNativeDateModule
  ],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    GeneralService
  ]
})
export class GeneralModule {
}
