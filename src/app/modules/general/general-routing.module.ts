import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TaskManagementComponent} from './task-management/task-management.component';

const routes: Routes = [
  {
    path: 'task', component: TaskManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeneralRoutingModule {
}
